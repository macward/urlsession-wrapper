//
//  NetworkingProtocols.swift
//  NetworkWrapper
//
//  Created by Max Ward on 08/06/2021.
//

import Foundation

enum RequestMethod: String {
    case get = "GET"
    case post = "POST"
    case put = "PUT"
    case patch = "PATCH"
    case delete = "DELETE"
}

typealias ReaquestHeaders = [String: String]
typealias RequestParameters = [String : Any?]
typealias ProgressHandler = (Float) -> Void

protocol RequestProtocol {
    var path: String { get }
    var method: RequestMethod { get }
    var headers: ReaquestHeaders? { get }
    var parameters: RequestParameters? { get }
}

protocol EnvironmentProtocol {
    var headers: ReaquestHeaders? { get }
    var baseURL: String { get }
}

protocol NetworkSessionProtocol {
    func dataTask(with request: URLRequest, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTask?
}

